# THRUST RIG01

This project involves the measuring of the thrust of a motor and propeller using a load cell,

COMPONENTS
i)   HX711 Weight Sensor Module
ii)  Load Cell
iii) Raspberry pi
iv)  Grand Turbo Brushless DC Motor
v)   Electronic Speed Controller(ESC)
vi)  Reset Push Button

The example.py file has code needed to read values from the load cell. The Motor.py file has code needed to drive the Grand TUrbo Brushless DC Motor. The Motor_Load_Cell.py file enables the user to control the motor while reading values from the load cell. Values read from the load cell are written to the values.csv file.

To start and run the Thrust_Rig, run the Motor_Load_Cell.py file as shown below:

python3 Motor_Load_Cell.py


  
